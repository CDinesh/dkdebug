# DKDebug


DKDebug is development debugging log tool which helps to control log in xcode project. 
### Version

 - 1.0.0 (LST)

DKDebug is in a swift version 4.2
### Installation

To install add below line in podfile of your project
```sh
pod 'DKDebug', :configurations => ['Debug']
```

### Use

1. import DKDebug in file 
```sh
import DKDebug
```
2. Assign value one time to namespaceAllowedArray which is string array.
```sh
namespaceAllowedArray = ["*"]
```
3. call log fuction which takes 2 parameters debugMessage and namespace
```sh
log(debugMessage: "Debug Message", namespace: "ABC")
```

### How it works
Library checks log namespace if namespace available in "namespaceAllowedArray" if available than it prints the log in console.

if "namespaceAllowedArray" contains * than it will print all log in console.
```sh
namespaceAllowedArray = ["*"]
```

You can segregate namespace with "|" pipe symbol.
```sh
"ABC|PQR"
```

You can also provide "*" with "|"
```sh
namespaceAllowedArray = ["ABC|*"]
```
Above will print all log with namespace starts with "ABC|", like below
```sh
log(debugMessage: "Debug Message", namespace: "ABC")
log(debugMessage: "Debug Message", namespace: "ABC|PQR")
log(debugMessage: "Debug Message", namespace: "ABC|PQR|XYZ")
log(debugMessage: "Debug Message", namespace: "ABC|XYZ")
log(debugMessage: "Debug Message", namespace: "PQR")
```

above will print
```sh
ABC|PQR: Debug Message
ABC|PQR|XYZ: Debug Message
ABC|XYZ: Debug Message
```

It will not print "ABC" namespace log, for "ABC" you need to specify it in array like
```sh
namespaceAllowedArray = ["ABC","ABC|*"]
```
 
It will also not work if "ABC*" is in array. "*" should be alone or after namespace with | *, like 
```sh
namespaceAllowedArray = [ABC|*"]
```
License
----

Apache version 2.0

**Cocoapod pod link Url**
https://cocoapods.org/pods/DKDebug

**Free Software, Hell Yeah!**

