//
//  DKDebug.swift
//  DKDebug
//
//  Created by Dinesh Choudhary on 25/03/20.
//  Copyright © 2020 Dinesh Choudhary. All rights reserved.
//

import Foundation

public var namespaceAllowedArray = NSArray()
public func log(debugMessage:String, namespace:String){
    if namespaceAllowedArray.contains("*") || namespaceAllowedArray.contains(namespace){
        print(namespace+": "+debugMessage)
        return
    }
    if namespaceAllowedArray.contains(where: {
        (namespace.hasPrefix(($0 as? String ?? "").replacingOccurrences(of: "*", with: "")) && (($0 as? String ?? "").suffix(2) == "|*"))
    }){
        print(namespace+": "+debugMessage)
        return
    }
}

