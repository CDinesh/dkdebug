Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '11.0'
s.name = "DKDebug"
s.summary = "A tiny Swift debugging utility."
s.requires_arc = true

# 2
s.version = "1.0.0"

# 3
s.license = { :type => "Apache License, Version 2.0", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Dinesh Choudhary" => "kdinesh.choudhary@gmail.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://gitlab.com/CDinesh/dkdebug"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://gitlab.com/CDinesh/dkdebug.git",
             :tag => "#{s.version}" }

# 7
s.framework = "UIKit"

# 8
s.source_files = "DKDebug/**/*.{swift}"

# 9
#s.resources = "../DKDebug/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

# 10
s.swift_version = "4.2"

end
